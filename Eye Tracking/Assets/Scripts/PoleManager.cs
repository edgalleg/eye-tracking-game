﻿using UnityEngine;
using System.Collections;

public class PoleManager : MonoBehaviour {

    public CapsuleCollider pole1;
    public CapsuleCollider pole2;
    public CapsuleCollider pole3;
    public CapsuleCollider pole4;
    public CapsuleCollider pole5;
    public CapsuleCollider pole6;
    public CapsuleCollider pole7;
    public CapsuleCollider pole8;
    public CapsuleCollider pole9;
    public CapsuleCollider pole10;
    public CapsuleCollider pole11;

    public CapsuleCollider coin1;
    public CapsuleCollider coin2;
    public CapsuleCollider coin3;
    public CapsuleCollider coin4;
    public CapsuleCollider coin5;
    public CapsuleCollider coin6;
    public CapsuleCollider coin7;
    public CapsuleCollider coin8;
    public CapsuleCollider coin9;
    public CapsuleCollider coin10;

    public CapsuleCollider secondCoin1;
    public CapsuleCollider secondCoin2;
    public CapsuleCollider secondCoin3;
    public CapsuleCollider secondCoin4;
    public CapsuleCollider secondCoin5;
    public CapsuleCollider secondCoin6;
    public CapsuleCollider secondCoin7;
    public CapsuleCollider secondCoin8;
    public CapsuleCollider secondCoin9;
    public CapsuleCollider secondCoin10;

    public GameObject leftWall;
    public GameObject rightWall;
    public GameObject endWall;
    public GameObject floor;
    public GameObject ethan;


    // Use this for initialization
    void Start () {
        float poleGap = PlayerPrefs.GetFloat("Pole Gap Distance");

        Debug.Log(poleGap);////////////

        pole1.transform.position = new Vector3(0, .72f, poleGap);
        pole2.transform.position = new Vector3(0, .72f, 2* poleGap);
        pole3.transform.position = new Vector3(0, .72f, 3 * poleGap);
        pole4.transform.position = new Vector3(0, .72f, 4 * poleGap);
        pole5.transform.position = new Vector3(0, .72f, 5 * poleGap);
        pole6.transform.position = new Vector3(0, .72f, 6 * poleGap);
        pole7.transform.position = new Vector3(0, .72f, 7 * poleGap);
        pole8.transform.position = new Vector3(0, .72f, 8 * poleGap);
        pole9.transform.position = new Vector3(0, .72f, 9 * poleGap);
        pole10.transform.position = new Vector3(0, .72f, 10 * poleGap);
        pole11.transform.position = new Vector3(0, .72f, 11 * poleGap);

        coin1.transform.position = new Vector3(0, .72f, poleGap + poleGap/2);
        coin2.transform.position = new Vector3(0, .72f, 2 * poleGap + poleGap / 2);
        coin3.transform.position = new Vector3(0, .72f, 3 * poleGap + poleGap / 2);
        coin4.transform.position = new Vector3(0, .72f, 4 * poleGap + poleGap / 2);
        coin5.transform.position = new Vector3(0, .72f, 5 * poleGap + poleGap / 2);
        coin6.transform.position = new Vector3(0, .72f, 6 * poleGap + poleGap / 2);
        coin7.transform.position = new Vector3(0, .72f, 7 * poleGap + poleGap / 2);
        coin8.transform.position = new Vector3(0, .72f, 8 * poleGap + poleGap / 2);
        coin9.transform.position = new Vector3(0, .72f, 9 * poleGap + poleGap / 2);
        coin10.transform.position = new Vector3(0, .72f, 10 * poleGap + poleGap / 2);

        secondCoin1.transform.position = new Vector3(0, .72f, poleGap + poleGap / 2);
        secondCoin2.transform.position = new Vector3(0, .72f, 2 * poleGap + poleGap / 2);
        secondCoin3.transform.position = new Vector3(0, .72f, 3 * poleGap + poleGap / 2);
        secondCoin4.transform.position = new Vector3(0, .72f, 4 * poleGap + poleGap / 2);
        secondCoin5.transform.position = new Vector3(0, .72f, 5 * poleGap + poleGap / 2);
        secondCoin6.transform.position = new Vector3(0, .72f, 6 * poleGap + poleGap / 2);
        secondCoin7.transform.position = new Vector3(0, .72f, 7 * poleGap + poleGap / 2);
        secondCoin8.transform.position = new Vector3(0, .72f, 8 * poleGap + poleGap / 2);
        secondCoin9.transform.position = new Vector3(0, .72f, 9 * poleGap + poleGap / 2);
        secondCoin10.transform.position = new Vector3(0, .72f, 10 * poleGap + poleGap / 2);

        rightWall.transform.localScale += new Vector3(0, 0, 2.2f* poleGap);
        rightWall.transform.position = new Vector3(10, 0, ethan.transform.position.z);

        leftWall.transform.localScale += new Vector3(0, 0, 2.2f* poleGap);
        leftWall.transform.position = new Vector3(-10, 0, ethan.transform.position.z );

        floor.transform.localScale += new Vector3(0, 0, 2.2f* poleGap);
        floor.transform.position = new Vector3(0, -.02f, ethan.transform.position.z );

        endWall.transform.position = new Vector3(0, 0, pole11.transform.position.z + .8f* poleGap);

    }

    // Update is called once per frame
    void Update () {
	
	}
}
