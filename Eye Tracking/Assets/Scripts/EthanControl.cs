﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EthanControl : MonoBehaviour {
	public GameObject player;

    public GameObject halfwayWall;
    public GameObject finishWall;

    public GameObject Coin1;
    public GameObject Coin2;
    public GameObject Coin3;
    public GameObject Coin4;
    public GameObject Coin5;
    public GameObject Coin6;
    public GameObject Coin7;
    public GameObject Coin8;
    public GameObject Coin9;
    public GameObject Coin10;

    public GameObject secondCoin1;
    public GameObject secondCoin2;
    public GameObject secondCoin3;
    public GameObject secondCoin4;
    public GameObject secondCoin5;
    public GameObject secondCoin6;
    public GameObject secondCoin7;
    public GameObject secondCoin8;
    public GameObject secondCoin9;
    public GameObject secondCoin10;

    public float acceleration;
	public float maxSpeed;
	public float minSpeed;
	public float speed;
	public float sideSpeed = .07f;

    public int coins = 0;
    public Text coinsText;

    public Text timerLabel;
    public float time;

    bool started;
    bool secondHalf = false;

    private string outputText = "";

    // Use this for initialization
    void Start () {
        PlayerPrefs.SetInt("Coins", 0);
        acceleration = PlayerPrefs.GetFloat("Acceleration")/1000;
        maxSpeed = PlayerPrefs.GetFloat("Top Speed")/10;
        minSpeed = -PlayerPrefs.GetFloat("Top Speed")/10;

        if (acceleration > maxSpeed)
            acceleration = maxSpeed;

        finishWall.transform.position = new Vector3(0, 0, 0);
        finishWall.SetActive(false);

        //add output text header
        outputText += PlayerPrefs.GetString("Name") + " ";
        outputText += "Acceleration:" + (acceleration*1000).ToString() + " ";
        outputText += "TopSpeed:" + (maxSpeed * 10).ToString() + " ";
        outputText += "PoleGapDistance:" + PlayerPrefs.GetFloat("Pole Gap Distance").ToString() + " ";
    }

	// Update is called once per frame
	void Update () {

		float moveToSide = 0;

		Vector3 pos = player.transform.position;
        if (!secondHalf)
        {
            if (Input.GetKey("w"))
            {
                if (speed < maxSpeed)
                {
                    speed += acceleration;
                    started = true;
                }
            }
            else if (speed > 0)
            {
                speed -= acceleration;
                if (speed < 0)
                    speed = 0;
            }

            if (Input.GetKey("s"))
            {
                if (speed > minSpeed)
                {
                    speed -= acceleration;
                    started = true;
                }

            }
            else if (speed < 0)
            {
                speed += acceleration;
                if (speed > 0)
                    speed = 0;
            }

            if (Input.GetKey("a"))
            {
                moveToSide = -sideSpeed;
                started = true;
            }
            if (Input.GetKey("d"))
            {
                moveToSide = sideSpeed;
                started = true;
            }
        } else
        {
            if (Input.GetKey("s"))
            {
                if (speed < maxSpeed)
                {
                    speed += acceleration;
                    started = true;
                }

            }
            else if (speed > 0)
            {
                speed -= acceleration;
                if (speed < 0)
                    speed = 0;
            }

            if (Input.GetKey("w"))
            {
                if (speed > minSpeed)
                {
                    speed -= acceleration;
                    started = true;
                }

            }
            else if (speed < 0)
            {
                speed += acceleration;
                if (speed > 0)
                    speed = 0;
            }

            if (Input.GetKey("d"))
            {
                moveToSide = -sideSpeed;
                started = true;
            }
            if (Input.GetKey("a"))
            {
                moveToSide = sideSpeed;
                started = true;
            }
        }

        if (started)
        {
            time += Time.deltaTime;

            //timer messes up after 2 minutes. need to fix
            var minutes = time / 120; //Divide the guiTime by sixty to get the minutes.
            var seconds = time % 60;//Use the euclidean division for the seconds.
            var fraction = (time * 100) % 100;

            //update the label value
            timerLabel.text = string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);
            if (timerLabel.text.Substring(12) == "0")
                outputText += "("+ player.transform.position.x.ToString() + "," + player.transform.position.z.ToString() + ") ";
        }
        //player.transform.position = new Vector3 (pos.x+moveToSide, pos.y, pos.z + speed);
        //player.transform.rotation.x * speed
        Vector3 dir = new Vector3(0, 0, 1);
        dir = Quaternion.Euler(player.transform.rotation.eulerAngles) * dir;
        dir *= speed;
        player.transform.position = new Vector3(pos.x + moveToSide, pos.y, pos.z);
        player.transform.position += dir;

    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name.Contains("Pole"))
            speed = 0;
        if (col.gameObject.name.Contains("Coin")) {
            Destroy(col.gameObject);
            coins += 1;
            coinsText.text = "Coins: " + coins.ToString();
        }
        if (col.gameObject.name.Contains("Halfway")) {
            startSecondHalf();
    }
    }
	void OnCollisionStay (Collision col){
		speed = 0;
        if (col.gameObject.name.Contains("Coin"))
        {
            Destroy(col.gameObject);
            coins += 1;
            coinsText.text = "Coins: " + coins.ToString();
        }
        else if (col.gameObject.name.Contains("Finish"))
        {
            PlayerPrefs.SetInt("Coins", coins);
            PlayerPrefs.SetString("Time", timerLabel.text);
            SceneManager.LoadScene(2);
            string nowtime = System.DateTime.Now.ToString("HHmm");
            var file = System.IO.File.CreateText("C:\\" + PlayerPrefs.GetString("Name") + "_" + nowtime + ".txt");
            file.Close();
            outputText += "Coins:" + coins.ToString()+ " ";
            outputText += "Time:" + timerLabel.text;
            System.IO.File.WriteAllText("C:\\" + PlayerPrefs.GetString("Name") +"_"+ nowtime+ ".txt", outputText);
        }
    }

    public void restart()
    {
        SceneManager.LoadScene(1);
    }

    void startSecondHalf()
    {
        player.transform.rotation = new Quaternion(0, 180, 0,0); //turns player around
        secondHalf = true;
        Destroy(halfwayWall);
        Destroy(Coin1);
        Destroy(Coin2);
        Destroy(Coin3);
        Destroy(Coin4);
        Destroy(Coin5);
        Destroy(Coin6);
        Destroy(Coin7);
        Destroy(Coin8);
        Destroy(Coin9);
        Destroy(Coin10);

        finishWall.SetActive(true);
        secondCoin1.SetActive(true);
        secondCoin2.SetActive(true);
        secondCoin3.SetActive(true);
        secondCoin4.SetActive(true);
        secondCoin5.SetActive(true);
        secondCoin6.SetActive(true);
        secondCoin7.SetActive(true);
        secondCoin8.SetActive(true);
        secondCoin9.SetActive(true);
        secondCoin10.SetActive(true);
        speed = 0; 
    }
}
