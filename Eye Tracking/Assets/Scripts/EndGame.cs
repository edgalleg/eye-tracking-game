﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

    public Button replayButton;
    public Button mainMenuButton;

    public Text name;
    public Text time;
    public Text coins;

	// Use this for initialization
	void Start () {
        name.text = "Name: " + PlayerPrefs.GetString("Name");
        time.text = "Time: " +PlayerPrefs.GetString("Time");
        coins.text = "Coins: " + PlayerPrefs.GetInt("Coins").ToString();
    }

	
	// Update is called once per frame
	void Update () {
	
	}

    public void replayPressed()
    {
        SceneManager.LoadScene(1);
    }

    public void mainMenuPressed()
    {
        SceneManager.LoadScene(0);
    }
}
