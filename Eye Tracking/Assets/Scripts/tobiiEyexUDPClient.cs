﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;
using System.Text;
using System.Net;
using System.Collections;
using System.Collections.Generic;

public class tobiiEyexUDPClient : MonoBehaviour
{
    float rotate_x_display = 0f;
    public GameObject theEthan;
    public byte[] dataStream { get; private set; }
    public Socket clientSocket { get; private set; }
    int portNumber = 30000;

    // Use this for initialization
    void Start()
    {
        this.dataStream = new byte[255];
        this.connect();
    }

    // Update is called once per frame
    void Update()
    {
        theEthan.transform.Rotate(new Vector3(0, this.rotate_x_display * 2, 0));
        this.rotate_x_display = 0;
    }

    public enum DataIdentifier
    {
        HeartBeat,
        Null
    };

    public byte[] ToByte()
    {
        List<byte> result = new List<byte>();

        //First four are for the Command
        result.AddRange(BitConverter.GetBytes((int)0));

        //Add the length of the name
        result.AddRange(BitConverter.GetBytes(sizeof(double)));
        result.AddRange(BitConverter.GetBytes(sizeof(double)));

        //Add the name
        result.AddRange(BitConverter.GetBytes(0));
        result.AddRange(BitConverter.GetBytes(0));

        return result.ToArray();
    }

    private void ReceiveData(IAsyncResult asyncResult)
    {
        try
        {
            Debug.Log("Data Received.");

            // Initialise a packet object to store the data to be sent
            //Packet sendData = new Packet();

            // Initialise the IPEndPoint for the clients
            IPEndPoint clients = new IPEndPoint(IPAddress.Any, 0);

            // Initialise the EndPoint for the clients
            EndPoint epSender = (EndPoint)clients;

            // Receive all data
            this.clientSocket.EndReceiveFrom(asyncResult, ref epSender);

            int nameLength = BitConverter.ToInt32(dataStream, 4);

            // Read the length of the message (4 bytes)
            int msgLength = BitConverter.ToInt32(dataStream, 8);
            double valx = BitConverter.ToDouble(dataStream, 12);
            double valy = BitConverter.ToDouble(dataStream, 20);

            rotate_x_display = (float)valx;
            //Debug.Log("type[" + (DataIdentifier)BitConverter.ToInt32(dataStream, 0) + "]\t" + "x[" + valx + "]\t" + "y[" + valy + "]\r\n");

            // Listen for more connections again...
            this.clientSocket.BeginReceiveFrom(this.dataStream, 0, this.dataStream.Length, SocketFlags.None, ref epSender, new AsyncCallback(this.ReceiveData), epSender);
        }
        catch (Exception ex)
        {
            Console.WriteLine("ReceiveData Error: " + ex.Message, "UDP Server");
        }
    }

    public bool connect()
    {
        int debugCount = 0;
        try
        {
            // Initialise the socket
            clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            
            // Initialise the IPEndPoint for the server and listen on port 30000
            IPEndPoint client = new IPEndPoint(IPAddress.Any, 30002);
            
            // Associate the socket with this IP address and port
            clientSocket.Bind(client);
            
            // Initialise the EndPoint for the clients
            EndPoint epSender = (EndPoint)client;
            
            // Start listening for incoming data
            clientSocket.BeginReceiveFrom(dataStream, 0, dataStream.Length, SocketFlags.None, ref epSender, new AsyncCallback(ReceiveData), epSender);
            
            // Initialise the IPEndPoint for the clients
            IPEndPoint server = new IPEndPoint(IPAddress.Loopback, 30000);
            
            // Initialise the EndPoint for the clients
            EndPoint epSenderServer = (EndPoint)server;
            Debug.Log("" + debugCount++);

            byte[] data = this.ToByte();
            
            //clientSocket.BeginSendTo(data, 0, data.Length, SocketFlags.None, server, null, server);
            clientSocket.SendTo(data, epSenderServer);
            Debug.Log("" + debugCount++);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            Console.WriteLine(e.ToString());
        }

        return true;
    }
}