﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class udpdata : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    public enum DataIdentifier
    {
        HeartBeat,
        Null
    };

    //Default constructor
    public udpdata()
    {
        this.cmdCommand = DataIdentifier.Null;
        this.X = 0;
        this.Y = 0;
    }

    //Converts the bytes into an object of type Data
    public udpdata(byte[] data)
    {
        //The first four bytes are for the Command
        this.cmdCommand = (DataIdentifier)BitConverter.ToInt32(data, 0);

        //The next four store the length of the name
        int nameLen = BitConverter.ToInt32(data, 4);

        //The next four store the length of the message
        int msgLen = BitConverter.ToInt32(data, 8);

        //Makes sure that strName has been 
        //passed in the array of bytes
        this.X = BitConverter.ToDouble(data, 12);
        this.Y = BitConverter.ToDouble(data, 12 + nameLen);
        //if (nameLen > 0)
        //    this.X = Encoding.UTF8.GetString(data, 12, nameLen);
        //else
        //    this.X = null;

        ////This checks for a null message field
        //if (msgLen > 0)
        //    this.Y =
        //      Encoding.UTF8.GetString(data, 12 + nameLen, msgLen);
        //else
        //    this.Y = null;
    }

    //Converts the Data structure into an array of bytes
    public byte[] ToByte()
    {
        List<byte> result = new List<byte>();

        //First four are for the Command
        result.AddRange(BitConverter.GetBytes((int)cmdCommand));

        //Add the length of the name
        result.AddRange(BitConverter.GetBytes(sizeof(double)));
        result.AddRange(BitConverter.GetBytes(sizeof(double)));

        //Add the name
        result.AddRange(BitConverter.GetBytes(X));
        result.AddRange(BitConverter.GetBytes(Y));

        return result.ToArray();
    }

    //Name by which the client logs into the room
    public double X;
    //Message text
    public double Y;
    //Command type (login, logout, send message, etc)
    public DataIdentifier cmdCommand;
}
