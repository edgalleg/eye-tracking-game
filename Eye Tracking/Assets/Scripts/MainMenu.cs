﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class MainMenu : MonoBehaviour {

    public InputField nameField;
    public Slider accelerationSlider;
    public Slider topSpeedSlider;
    public Slider poleGapDistanceSlider;
    public Text accelerationText;
    public Text topSpeedText;
    public Text poleGapDistanceText;

    public Button submit;

    // Use this for initialization
    void Start () {
        topSpeedText.text = topSpeedSlider.value.ToString();
        accelerationText.text = accelerationSlider.value.ToString();
        poleGapDistanceText.text = poleGapDistanceSlider.value.ToString();

    }

    // Update is called once per frame
    void Update () {
        
    }

	public void submitClicked(){
        if (nameField.text.ToString() == "") {
            float rnum = Random.Range(0, 9999999);
            PlayerPrefs.SetString("Name", "Player" + rnum.ToString());
        }else {
            PlayerPrefs.SetString("Name", nameField.text.ToString());
        }
  
        PlayerPrefs.SetFloat("Top Speed", topSpeedSlider.value);
        PlayerPrefs.SetFloat("Acceleration", accelerationSlider.value);
        PlayerPrefs.SetFloat("Pole Gap Distance", poleGapDistanceSlider.value);
      
        SceneManager.LoadScene (1);
	}

    public void updatePoleGapDistance()
    {
        poleGapDistanceText.text = poleGapDistanceSlider.value.ToString();
    }

    public void updateTopSpeed()
    {
        topSpeedText.text = topSpeedSlider.value.ToString();
    }

    public void updateAcceleration()
    {
        accelerationText.text = accelerationSlider.value.ToString();
    }
}
